<?php


namespace Hexxondiv\Waba;


class Waba
{
    public $api_key;
    public $base_url;
    public $version;
    public $message_data;
    public $message_type;
    public $message_to;
    public $message_subject;
    public $recipient_type;
    public $body;
    public $typeList;
    public $from_company_name;


    public function __construct()
    {
        $this->api_key = "BckaIm_sandbox";
        $this->version = "v1";
        $this->base_url = "https://waba-sandbox.360dialog.io";
        $this->message_type = 'text';
        $this->message_subject = null;
        $this->recipient_type = 'individual';
        $this->from_company_name = null;
        $this->typeList = [
            'document',
            'text',
            'interactive',
            'contacts',
            'location',
        ];
    }

    /**
     * @param $data
     * Expected data: base_url,version, and api_key
     * @return $this
     */
    public function setCredentials($data)
    {
        if (isset($data['base_url']))
            $this->base_url = $data['base_url'];
        if (isset($data['api_key']))
            $this->api_key = $data['api_key'];
        $this->version = $data['version'] ?: 'v1';
        $this->buildUrl();
        return $this;
    }

    private function buildUrl()
    {
        $this->base_url .= '/' . $this->version . '/';
        return $this;
    }

    public function messageType($type = null)
    {
        if (is_null($type)) {
            return $this->message_type;
        }
        $this->message_type = $type;
        return $this;
    }

    /**
     * Sets/Gets Receiver ID (wa_id)
     * @param $wa_id
     * @return $this|| message_id
     */
    public function to($wa_id = null)
    {
        if (is_null($wa_id))
            return $this->message_to;
        $this->message_to = $wa_id;
        return $this;

    }

    public function from_($from_company_name = null)
    {
        if (is_null($from_company_name))
            return $this->from_company_name;
        $this->from_company_name = $from_company_name;
        return $this;
    }

    public function subject($message_subject = null)
    {
        if (is_null($message_subject))
            return $this->message_subject;
        $this->message_subject = $message_subject;
        return $this;
    }

    public function bodyText($body_text = null)
    {
        if (is_null($body_text))
            return $this->body;
        $this->body = $body_text;
        return $this;

    }

    /**
     * Sets Recipient Type for a whatsapp message.
     * @param string $recipient_type (default: individual)
     * @return string
     */
    public function recipientType($recipient_type = null)
    {
        if (is_null($recipient_type))
            return $this->recipient_type;
        $this->recipient_type = $recipient_type;
        return $this;
    }

    public function endpoint()
    {
        return $this->base_url;
    }

    public function buildSimpleSendData()
    {
        $msgBody = "";
        if (!is_null($this->from_company_name))
            $msgBody .= "*".$this->from_company_name . "*\n";
        if (!is_null($this->message_subject))
            $msgBody .= "*Subject:* " . $this->message_subject . "\n";
        if (!empty($msgBody))
            $msgBody .= "======================\n";
        $msgBody .= $this->body;


        $this->message_data = [
            'to' => $this->to(),
            'type' => $this->messageType(),
            'recipient_type' => $this->recipientType(),
            'text' => [
                "body" => $msgBody,
            ],
        ];
    }

    public function send($formatted = false)
    {
        $result = array();
        $url = $this->base_url . 'messages';
        $this->buildSimpleSendData();
        $data_string = json_encode($this->message_data);
//        dd($data_string);
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'D360-API-KEY:' . $this->api_key));
        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);

        curl_close($ch);

//        dd($response,$url);
        if ($response) {
            $result = json_decode($response, true);
        }
        return $formatted ? $this->formatted_result($result) : $result;
    }

    private function formatted_result($result)
    {
        $payload = [];
        if (isset($result['contacts']))
            $payload['wa_id'] = $result['contacts'][0]['wa_id'];
        if (isset($result['messages']))
            $payload['message_id'] = $result['messages'][0]['id'];
        if (isset($result['meta'])) {
            $payload['meta'] = $result['meta'];
        }
        return $payload;
    }


}
