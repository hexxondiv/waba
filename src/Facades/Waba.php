<?php
namespace Hexxondiv\Waba\Facades;

use Illuminate\Support\Facades\Facade;

class Waba extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'waba';
    }
}
